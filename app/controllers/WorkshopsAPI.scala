package controllers

import controllers.Application._
import model.{Training, TrainingDAO}
import org.bson.types.ObjectId
import org.joda.time.DateTime
import play.api.Logger
import play.api.libs.json._
import play.api.mvc.Controller

/**
 * Created by Szymon on 06/06/2015.
 */
object WorkshopsAPI extends Controller{

  val daoTrainings = new TrainingDAO(context.training)


  implicit  val objectIdFormat: Format[ObjectId] = new Format[ObjectId] {
    def reads(json: JsValue) = {
      json match {
        case jsString: JsString => {
          if ( ObjectId.isValid(jsString.value) ) JsSuccess(new ObjectId(jsString.value))
          else JsError("Invalid ObjectId")
        }
        case other => JsError("Can't parse json path as an ObjectId. Json content = " + other.toString())
      }
    }
    def writes(oId: ObjectId): JsValue = {
      JsString(oId.toString)
    }
  }

  implicit object trainingFormat extends Format[Training] {
    def reads(json: JsValue) = JsSuccess(Training(
      Some((json \ "id").asOpt[ObjectId].getOrElse(new ObjectId())),
      (json \ "title").as[String],
      (json \ "image").as[String],
      (json \ "startDate").asOpt[DateTime],
      (json \ "description").as[String],
      (json \ "place").as[String],
      (json \ "speakers").as[List[String]],
      (json \ "participants").asOpt[List[String]],
      (json \ "limitOfPersons").as[Int],
      (json \ "createdAt").asOpt[DateTime]
    ))

    def writes(training: Training) = Json.obj(
      "id" -> training.id,
      "title" -> training.title,
      "image" -> training.image,
      "startedDate" -> training.startDate,
      "description" -> training.description ,
      "place" -> training.place,
      "speakers" -> training.speakers,
      "participants" -> training.participants,
      "limitOfPersons" -> training.limitOfPersons,
      "createdAt" -> training.createdAt
    )
  }

  //http://ethanway.com/mongodb-with-scala/
  /*
  Simple GET ALL
   */
  def query = CORSFilter {
    val trainingsJson = Json.obj("trainings" -> daoTrainings.find().toList)
    Logger.info("Return JSON: " + trainingsJson)
    Ok(trainingsJson)
  }

  def getOne(id: String) = CORSFilter {
    val trainingsJson = Json.obj("training" -> daoTrainings.findOne(new ObjectId(id)).getOrElse(throw new RuntimeException("Object doesn't exist")))
    Logger.info("Return JSON: " + trainingsJson)
    Ok(trainingsJson)
  }

  /*
  Just for AngularJS, OPTION request
   */
  def checkPreFlight = CORSFilter {
    NoContent
  }

  def createWorkshop = CORSFilter(parse.json){  request =>
    Logger.info("From server: " + request.body)

    val result =  request.body.validate[Training].map(
      workshop =>
        daoTrainings.insert(workshop.copy(createdAt = Some(DateTime.now())))
    ).getOrElse(Logger.info("Invalid Json"))

    Logger.info("Object: " + result + " added to database")

    Created("WyniK: " + result)
  }

  /*
  DELETE workshop
   */
  def removeWorkshop(id: String) = CORSFilter {
    val numberOfRemovedObjects = daoTrainings.delete(new ObjectId(id))
    Logger.info("Number of removed objects: " + numberOfRemovedObjects)
    NoContent
  }

  /*
  UPDATE workshop, TODO compare with database if needed!
   */
  def updateWorkshop(id: String) = CORSFilter(parse.json){  request =>

    val result = daoTrainings.update(new ObjectId(id), request.body.validate[Training].get)

    Logger.info("Updated object: " + result)
    Ok("Updated object: " + result)
  }

}
