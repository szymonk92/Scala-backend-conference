package controllers

import controllers.Application._
import model.{TrainingDAO, Visitor, VisitorsDAO}
import org.bson.types.ObjectId
import org.joda.time.DateTime
import play.api.Logger
import play.api.libs.json._
import play.api.mvc.Controller

/**
 * Created by Szymon on 06/06/2015.
 */
object VisitorsAPI extends Controller {
  /*
  Logger <- in DAO should be 'trace', not 'info', but it's just easier for tests
   */

  val daoVisitors = new VisitorsDAO(context.visitors)
  val daoTrainings = new TrainingDAO(context.training)

  implicit object visitorFormat extends Format[Visitor] {
    def reads(json: JsValue) = JsSuccess(Visitor(
      Some((json \ "id").asOpt[ObjectId].getOrElse(new ObjectId())),
      (json \ "firstName").as[String],
      (json \ "lastName").as[String],
      (json \ "email").as[String],
      (json \ "birthday").asOpt[DateTime]
    ))

    def writes(visitor: Visitor) = Json.obj(
      "id" -> visitor.id,
      "firstName" -> visitor.firstName,
      "lastName" -> visitor.lastName,
      "email" -> visitor.email,
      "birthday" -> visitor.birthday
    )
  }

  def visitorsList() = CORSFilter {
    val visitorsList = daoVisitors.find()
    val visitors = visitorsList.toList;
    Logger.info("Visitors: " + visitors)

    Ok(Json.obj("visitors" -> visitors))
  }

  def getOne(id: String) = CORSFilter {
    val trainingsJson = Json.obj("training" -> daoVisitors.findOne(new ObjectId(id)).get)
    Logger.info("Return JSON: " + trainingsJson)
    Ok(trainingsJson)
  }

  def checkPreFlight() = CORSFilter{
    NoContent
  }

  def createVisitor()= CORSFilter(parse.json){  request =>
    Logger.info("From server: " + request.body)

    val result =  request.body.validate[Visitor].map(
      workshop =>
        daoVisitors.insert(workshop)
    ).getOrElse(Logger.info("Invalid Json"))

    Logger.info("Object: " + result + " added to database")

    Created("Added to database object: " + result)
  }

  def addToWorkshop(id: String)= CORSFilter(parse.json){  request =>
    Logger.info("From server: " + request.body)

    val visitor  = daoVisitors.insert(request.body.validate[Visitor].get)
    val workshop = daoTrainings.findOne(new ObjectId(id)).getOrElse(throw new RuntimeException("Object doesn't exist"))

    daoTrainings.update(new ObjectId(id), workshop.copy(participants = Option(workshop.participants.getOrElse(List()) :+ visitor.id.get.toString))) //LOL

    Created("Visitor: " + visitor + " has been created" + " and added to workshop: " + workshop)
  }


}


