package controllers


import java.util.Properties
import com.mongodb.ServerAddress
import com.mongodb.casbah.{MongoClient, MongoDB, MongoCredential}
import org.slf4j.LoggerFactory

/**
 * Created by Szymon on 26/05/2015.
 */
class MongoContext {

  def LOG = LoggerFactory.getLogger(this.getClass.getName)

  private var _db: MongoDB = null

  lazy val visitors = _db("visitors")
  lazy val training = _db("training")

  def connect() {
    val properties = loadProperties()

    val host = properties.getProperty("host")
    val port = properties.getProperty("port")
    val username = properties.getProperty("username")
    val password = properties.getProperty("password")
    val dbName = properties.getProperty("dbName")

    val server = new ServerAddress(host, port.toInt)
    val credentials = MongoCredential.createCredential(username, dbName, password.toCharArray)
    val client = MongoClient(server, List(credentials))
    client(dbName)
    _db = client(dbName)

    LOG.info("Connected with mongoDB")
  }

  def loadProperties(fileName: String = "/mongo.properties"): Properties = {
    val properties = new Properties()

    LOG.debug("Loading properties")
    properties.load(
      getClass.getResourceAsStream(fileName))

    properties
  }
}
