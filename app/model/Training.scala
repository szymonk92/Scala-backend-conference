package model

import com.mongodb.DBObject
import com.mongodb.casbah.Imports._
import com.mongodb.casbah.commons.MongoDBObject
import org.bson.types.ObjectId
import org.joda.time.DateTime
import play.api.libs.json.{Reads, Writes}


/**
 * Created by Szymon on 26/05/2015.
 */
//2015-05-28T19:18:00.430+02:00
//http://stackoverflow.com/questions/20616677/defaults-for-missing-properties-in-play-2-json-formats

case class Training(
                     id: Option[ObjectId] = Some(new ObjectId()),
                     title: String,
                     image: String,
                     startDate: Option[DateTime],
                     description: String,
                     place: String,
                     speakers: List[String],
                     participants: Option[List[String]],
                     limitOfPersons: Int,
                     createdAt: Option[DateTime]
                     )


object TrainingMap {

  def toBson(training: Training): DBObject = {
    MongoDBObject(
      "_id" -> training.id.get,
      "title" -> training.title,
      "image" -> training.image,
      "startDate" -> training.startDate,
      "description" -> training.description,
      "place" -> training.place,
      "speakers" -> training.speakers,
      "participants" -> training.participants,
      "limitOfPersons" -> training.limitOfPersons,
      "createdAt" -> training.createdAt
    )
  }

  def fromBson(o: DBObject): Training = {
    Training(
      id = Some(o.as[ObjectId]("_id")),
      title = o.as[String]("title"),
      image = o.as[String]("image"),
      startDate = Option(o.as[DateTime]("startDate")), // startedDate = Some(o.as[DateTime]("startDate")).flatMap(x => if( x == null) None else Some(x)),
      description = o.as[String]("description"),
      place = o.as[String]("place"),
      speakers = o.as[List[String]]("speakers"),
      participants = Option(o.as[List[String]]("participants")), // Option when might not be in database
      limitOfPersons = o.as[Int]("limitOfPersons"),
      createdAt = Some(o.as[DateTime]("createdAt"))
    )
  }
}


