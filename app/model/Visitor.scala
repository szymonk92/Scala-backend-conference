package model

import com.mongodb.DBObject
import com.mongodb.casbah.Imports._
import com.mongodb.casbah.commons.MongoDBObject
import org.bson.types.ObjectId
import org.joda.time.DateTime

/**
 * Created by Szymon on 25/05/2015.
 */

case class Visitor(
                    id: Option[ObjectId] = Some(new ObjectId()),
                    firstName: String,
                    lastName: String,
                    email: String,
                    birthday: Option[DateTime]
                    )


object VisitorsMap {
  def toBson(visitor: Visitor): DBObject = {
    MongoDBObject(
      "_id" -> visitor.id,
      "firstName" -> visitor.firstName,
      "lastName" -> visitor.lastName,
      "email" -> visitor.email,
      "birthday" -> visitor.birthday)
  }

  def fromBson(o: DBObject): Visitor = {
    Visitor(
      id = Some(o.as[ObjectId]("_id")),
      firstName = o.as[String]("firstName"),
      lastName = o.as[String]("lastName"),
      email = o.as[String]("email"),
      birthday = Option(o.as[DateTime]("birthday"))
    )
  }

}
