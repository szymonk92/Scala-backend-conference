package model

import com.mongodb.casbah.MongoCollection
import org.bson.types.ObjectId

/**
 * Created by Szymon on 25/05/2015.
 */
class VisitorsDAO(mongoCollection: MongoCollection) {

  def find(): Vector[Visitor] = {
    mongoCollection.find().toVector
      .map(VisitorsMap.fromBson)
  }

  def findOne(id: ObjectId): Option[Visitor] = {
    val doc = mongoCollection.findOneByID(id)
      .getOrElse(return None)
    Some(VisitorsMap.fromBson(doc))
  }

  def insert(visitor: Visitor): Visitor = {
    val doc = VisitorsMap.toBson(visitor)
    mongoCollection.insert(doc)
    VisitorsMap.fromBson(doc)
  }


}
