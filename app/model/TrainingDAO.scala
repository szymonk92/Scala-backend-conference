package model

import com.mongodb.casbah.MongoCollection
import com.mongodb.casbah.commons.MongoDBObject
import org.bson.types.ObjectId

/**
 * Created by Szymon on 26/05/2015.
 */
class TrainingDAO(mongoCollection: MongoCollection) {

  def insert(training: Training): Training = {
    val doc = TrainingMap.toBson(training)
    mongoCollection.insert(doc)
    TrainingMap.fromBson(doc)
  }

  def find(): Vector[Training] = {
    mongoCollection.find().toVector
      .map(TrainingMap.fromBson)
  }

  def findOne(id: ObjectId): Option[Training] = {
    val doc = mongoCollection.findOneByID(id)
      .getOrElse(return None)
    Some(TrainingMap.fromBson(doc))
  }

  /*
  Return number of deleted elements
   */
  def delete(id: ObjectId): Int = {
    val q = MongoDBObject("_id" -> id)
    val result = mongoCollection.remove(q)
    result.getN
  }

  def update(id: ObjectId, artist: Training): Training = {
    val q = MongoDBObject("_id" -> id)
    println(q)
    val doc = TrainingMap.toBson(artist)
    mongoCollection.update(q, doc)
    TrainingMap.fromBson(doc)
  }
}

