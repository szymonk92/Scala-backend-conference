package model

/**
 * Created by Szymon on 25/05/2015.
 */
object AuthorizationType extends Enumeration {
  val Facebook, Google, MyWebsite = Value
}


////sealed mozna rozszerza? w przeciwie?stwie do abstarct
//sealed case class Planet(masa: Int)
//case object Ziemia extends Planet(400000)
//case object Pluton extends Planet(30000)
//case object Neptun extends Planet(500000)
//
//object testowy {
//  def poco(planeta: Planet): Unit = {
//    planeta match {
//      case Ziemia => println(" WOW, ale wielk, ma: " + Ziemia.masa)
//      case Pluton =>
//      case Neptun =>
//    }
//  }
//}
//
//
//sealed case class Planet2( masa: Int )
//case class Ziemia2( luczie : Int) extends Planet(400000)
//case class Pluton2( zimno : Double) extends Planet(30000)
//case class Neptun2() extends Planet(500000)
//
//object testowy2 {
//
//  def poco( planeta : Planet2): Unit ={ planeta match {
//    case Ziemia2(1000) => println(1000 + "sa beznadziejni")
//    case Ziemia2(x) if x>1000 => println(1000 + "sa beznadziejni")
//    case Ziemia2(x) if x<1000 => println(1000 + "nie jest tak zle")
//    case Pluton2(zimno) =>
//    case Neptun2() =>
//  }
//
// // poco(Ziemia2(23))
//  }
//}
