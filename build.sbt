name := """my-first-app"""

version := "1.0.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache,
  ws,
  filters,
  "org.mongodb" %% "casbah" % "2.8.1",
  "org.jsoup" % "jsoup" % "1.7.2",
  "joda-time" % "joda-time" % "2.7"
)
